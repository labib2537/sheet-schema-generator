<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schema_sheets', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->string('package_id')->nullable();
            $table->string('entity')->nullable();
            $table->string('connection_name')->nullable();
            $table->string('migration_file')->nullable();
            $table->text('json_migration')->nullable();
            $table->tinyInteger('is_generated')->default(1);
            $table->string('has_relation')->nullable();
            $table->string('relation_type')->nullable();
            $table->tinyInteger('is_master')->default(1);
            $table->string('pivot_table_name')->nullable();
            $table->mediumText('remarks')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamp('deleted_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schema_sheets');
    }
};
