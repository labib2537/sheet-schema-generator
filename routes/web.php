<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SchemaSheetController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('sheet', [SchemaSheetController::class, 'transformExcelToJson'])->name('all-data');
Route::post('createJson', [SchemaSheetController::class, 'create'])->name('create_json');
Route::post('store', [SchemaSheetController::class, 'store'])->name('json_store');
Route::post('select', [SchemaSheetController::class, 'selectTable'])->name('json_select');
Route::get('create', [SchemaSheetController::class, 'input'])->name('inputSpreadSheet');