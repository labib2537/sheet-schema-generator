<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
  @if($errorMessage)
    <p class="my-3">Spreadsheet ID has not Found!!</p>
  @else  
  <div class="heading" style="text-align: center;">
    <h4 class="my-3">Select Table</h4>
  </div>

  <form action="{{ route('create_json') }}" method="post">
    @csrf
    <div class="mb-3">
    <label for="spreadSheet" class="form-label">Input Spreadsheet ID</label>
    <input type="text" class="form-control" name="spreadSheet" value="{{$spreadsheet_id}}">
    </div>
    <select class="form-select" name="sheet" aria-label="Default select example">
    <option selected>Select Sheet</option>
    @foreach($tableData as $key=>$table)
    <option value="{{$table}}">{{$table}}</option>
    @endforeach
    </select>
    <button type="submit" class="btn btn-primary my-3">Show</button>
    </form>
    @endif
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
  </body>
</html>