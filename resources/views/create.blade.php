<?php
$jsonString = json_encode($jsonData);
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JSON STORE</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
  
  <div class="heading" style="text-align: center;">
    <h4 class="my-3">JSON DATA STORE</h4>
  </div>

    <form action="{{ route('json_store') }}" method="post">
        @csrf
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Pakeage ID</label>
    <input type="text" class="form-control" name="pkg_id" value="{{ $jsonResponse->headers->get('package-id') }}" readonly>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Entity</label>
    <input type="text" class="form-control" name="entity" value="{{ $jsonData->tableName }}" readonly>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Connection Name</label>
    <input type="text" class="form-control" name="conn_name" value="{{ $jsonData->connection }}" readonly>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Json Migration</label>
    <input type="text" class="form-control" name="json_migration" value="{{ $jsonString }}" readonly>
  </div>
  <button type="submit" class="btn btn-primary my-3">STORE</button>
</form>
<pre id="jsonContent"><?php echo htmlspecialchars($jsonString, ENT_QUOTES, 'UTF-8'); ?></pre>
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script>
    var jsonString = <?php echo json_encode($jsonString); ?>;
    document.getElementById("jsonContent").textContent = JSON.stringify(JSON.parse(jsonString), null, 2);
  </script>
</body>
</html>